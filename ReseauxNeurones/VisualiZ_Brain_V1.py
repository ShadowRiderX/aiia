
import numpy as np
import pandas as pd

import plotly.express as px
import plotly.graph_objects as go


# Données d'entrer : coordonnées polaire du phénomène
x_entrer = np.array(([1, 0.5], [2, 1], [3, 1.5], [4, 2], [0.5, 1],
                     [1.5, 2], [3, 2.5], [4, 3], [5, 2], [6, 3]),
                    dtype=float)

# Données de sortie : résultas avec 1 = Oui et 0 = Non
y = np.array(([1], [1], [1], [1], [0], [0], [0], [0], [0]), dtype=float)

# Nombre d'information en sortie
NB = len(y)

# Changement de l'échelle de nos valeurs pour être entre 0 et 1
# On divise chaque entré par la valeur max des entrées
x_entrer = x_entrer/np.amax(x_entrer, axis=0)

# On récupère ce qu'il nous intéresse
# Données sur lesquelles on va s'entrainer, les 8 premières de notre matrice
X = np.split(x_entrer, [NB])[0]
# Valeur que l'on veut trouver
xPrediction = np.split(x_entrer, [NB])[1]

# Notre classe de réseau neuronal
class Neural_Network(object):
    def __init__(self):

        # Nos paramètres
        # Nombre de neurones d'entrer
        self.inputSize = 2
        # Nombre de neurones de sortie
        self.outputSize = 1
        # Nombre de neurones cachés
        self.hiddenSize = 3

        # Nos poids
        # (2x3) Matrice de poids entre les neurones d'entrer et cachés
        self.W1 = np.random.randn(self.inputSize, self.hiddenSize)
        # (3x1) Matrice de poids entre les neurones cachés et sortie
        self.W2 = np.random.randn(self.hiddenSize, self.outputSize)


    # Fonction de propagation avant
    def forward(self, X):

        # Multiplication matricielle entre les valeurs d'entrer et les poids W1
        self.z = np.dot(X, self.W1)
        # Application de la fonction d'activation (Sigmoid)
        self.z2 = self.sigmoid(self.z)
        # Multiplication matricielle entre les valeurs cachés et les poids W2
        self.z3 = np.dot(self.z2, self.W2)
        # Application de la fonction d'activation, et obtention de notre valeur de sortie final
        o = self.sigmoid(self.z3)
        return o

    # Fonction d'activation
    def sigmoid(self, s):
        return 1/(1+np.exp(-s))

    # Dérivée de la fonction d'activation
    def sigmoidPrime(self, s):
        return s * (1 - s)

    # Fonction de rétropropagation
    def backward(self, X, y, o):

        # Calcul de l'erreur
        self.o_error = y - o
        # Application de la dérivée de la sigmoid à cette erreur
        self.o_delta = self.o_error*self.sigmoidPrime(o)

        # Calcul de l'erreur de nos neurones cachés
        self.z2_error = self.o_delta.dot(self.W2.T)
        # Application de la dérivée de la sigmoid à cette erreur
        self.z2_delta = self.z2_error*self.sigmoidPrime(self.z2)

        # On ajuste nos poids W1
        self.W1 += X.T.dot(self.z2_delta)
        # On ajuste nos poids W2
        self.W2 += self.z2.T.dot(self.o_delta)

    # Fonction d'entrainement
    def train(self, X, y):

        o = self.forward(X)
        self.backward(X, y, o)

    # Fonction de prédiction
    def predict(self):

        print("Donnée prédite apres entrainement: ")
        print("Entrée : \n" + str(xPrediction))
        print("Sortie : \n" + str(self.forward(xPrediction)))

        if(self.forward(xPrediction) < 0.5):
            print("La prediction est Non ! \n")
        else:
            print("La prediction est Oui ! \n")


NN = Neural_Network()

# List
ENTRER = []
SORTIE = []
PREDICTION = []
# Choisissez un nombre d'itération, attention un trop grand nombre peut créer un overfitting !
for i in range(1000):
    print("# " + str(i) + "\n")
    print("Valeurs d'entrées: \n" + str(X))
    ENTRER.append(X)
    print("Sortie actuelle: \n" + str(y))
    SORTIE.append(y)
    print("Sortie prédite: \n" + str(np.matrix.round(NN.forward(X), 2)))
    PREDICTION.append(np.matrix.round(NN.forward(X), 2))
    print("\n")
    NN.train(X, y)

NN.predict()


# Graphique du réseau de neurones
# Concaténation des trois listes en un seul tableau numpy
X_ENTRER = np.concatenate(ENTRER)
Y_SORTIE = np.concatenate(SORTIE)
Y_PREDICTION = np.concatenate(PREDICTION)

# Création du DataFrame
df = pd.DataFrame({'X_ENTRER': X_ENTRER[:, 0], 'Y_SORTIE': Y_SORTIE[:, 0],
                   'Y_PREDICTION': Y_PREDICTION[:, 0]})

# Création du graphique 3D
Graph = [go.Scatter3d(
        x=df['X_ENTRER'], y=df['Y_SORTIE'], z=df['Y_PREDICTION'],
        mode='lines+markers',
        marker=dict(
                size=6,
                color=df['Y_PREDICTION'],
                colorscale='Viridis',
                opacity=0.8
            ),
        line=dict(color='black', width=2)
        )
    ]

layout = go.Layout(
    title="Graphique du réseau de neurones",
)

fig = go.Figure(data=Graph, layout=layout)

fig.update_layout(
    scene=dict(
        xaxis=dict(title='ENTRER', nticks=4, range=[-1, 1],),
        yaxis=dict(title='SORTIE', nticks=4, range=[-1, 1],),
        zaxis=dict(title='PREDICTION', nticks=4, range=[-1, 1],),
    )
)

fig.show()


