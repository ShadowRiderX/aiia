# Réseaux de neurones

Le script Brain_V1 reconstitue un réseau de neurone afin de prédire la valeur d'un résultat binaire pour la dernière période d'un phénomène.  

Adméttons que l'on connait les coordonnées polaire d'un phénomène lambda.  
et les résultats sur l'ensemble des périodes sauf le dernier.  

La variable x_entrer correspond aux coordonnées polaire du phénomène.  
On connait les coordonnées de toutes les entrées.  

La variable y correspond aux résultats de la situation.  
On ne connait pas le résultat pour la dernière situation.  

Ce réseau de neurone prend comme fonction d'activation la fonction de répartition de la loi Logistique, appelé fonction Sigmoïde.  

