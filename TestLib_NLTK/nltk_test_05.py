# Sources : https://www.holisticseo.digital/python-seo/nltk/wordnet

# Synonymes : Se dit de mots ou d'expressions qui ont un sens identique ou très voisin.
# Antonyme : Mot qui, par le sens, s'oppose directement à un autre (opposé à synonyme).

lang = 'fra'

def synonym_antonym_extractor(phrase):
     from nltk.corpus import wordnet
     synonyms = []
     antonyms = []

     for syn in wordnet.synsets(phrase, lang=lang):
          for l in syn.lemmas(lang):
               synonyms.append(l.name().lower())
               if l.antonyms():
                    antonyms.append(l.antonyms()[0].name())

     print(f' synonyms : {set(synonyms)}')
     print(f' antonyms : {set(antonyms)}')


synonym_antonym_extractor(phrase="ouvrage")

