from nltk.corpus import stopwords
from nltk import word_tokenize
from nltk.tokenize import sent_tokenize
import re

import nltk

data = u"""Wikipédia est un projet wiki d’encyclopédie collective en ligne, universelle, multilingue et fonctionnant sur le principe du wiki. Aimez-vous l'encyclopédie wikipedia ?"""

french_stopwords = set(stopwords.words('french'))
filtre_stopfr = lambda text: [token for token in text if token.lower() not in french_stopwords]

A = filtre_stopfr(word_tokenize(data, language="french"))
print(A)

sp_pattern = re.compile( """[\.\!\"\s\?\-\,\']+""", re.M).split
B = sp_pattern(data)
print(B)

C = sent_tokenize(data, language="french")
print(C)

D = word_tokenize(data, language="french")
print(D)

E = filtre_stopfr(word_tokenize(data, language="french"))
print(E)

fd = nltk.FreqDist(A)
print(fd.most_common())
