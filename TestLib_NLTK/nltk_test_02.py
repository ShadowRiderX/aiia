import nltk
from nltk.corpus import wordnet as wn

nltk.download("wordnet", quiet=True)

synset_array = wn.synsets('book')
print(synset_array)

i = 0
for element in synset_array:
    # Definition = Definition
    definition = synset_array[i].definition()

    # Lemma_name = Lemme
    # Une chaîne de signes formant une unité sémantique et pouvant constituer une entrée de dictionnaire
    lemma_name = synset_array[i].lemma_names()

    # Hyponyms = Hyponymes
    # Mot dont le sens (sa compréhension sémantique) est plus spécifique que celui d’un autre.
    # Exemple : Pouce est un hyponyme de doigt. Destrier est un hyponyme de cheval.
    hyponyms = synset_array[i].hyponyms()

    # Hypernyms = Hyperonymes
    # Catégorie générique, mot dont le sens inclut celui d’un autre mot.
    # Exemple : Ainsi, oiseau est un hyperonyme de mouette ou de corbeau, et mouette et corbeau sont des hyponymes d’oiseau.
    hypernyms = synset_array[i].hypernyms()

    # Meronyms = Méronymes
    # La méronymie est une relation sémantique entre mots, lorsqu'un terme désigne une partie d'un second terme.
    # Exemple, bras est un méronyme de corps, de même que toit est un méronyme de maison.
    # Meronyms = synset_array[i].meronyms()

    # Holonyms = Holonymes (La relation inverse est la méronymie)
    # L'holonymie est une relation sémantique entre mots d'une même langue.
    # Exemple : corps est un holonyme de bras, maison est un holonyme de toit.
    # holonyms = synset_array[i].holonyms()

    # Part Meronyms = partie méronymes ???
    part_meronyms = synset_array[i].part_meronyms()

    # Sisterm Terms = Termes soeur = Synonimes ???
    # Sister Language = Langue soeur = Certain mots français viennent du grec
    # https://en.wikipedia.org/wiki/Sister_language
    # sisterm_terms = synset_array[i].sisterm_terms()

    # Troponyms = Troponymes
    # La troponymie est la présence d'une relation de « manière » entre deux lexèmes.
    # Exemple : Grignoter, c'est manger d'une certaine manière, et se gorger, c'est manger d'une manière différente.
    # troponyms = synset_array[i].troponyms()

    # Inherited Hypernyms = hyperonymes hérités
    # inherited_hypernyms = synset_array[i].inherited_hypernyms()

    # synonymous synonyms = synonymes
    # synonyms = synset_array[i].synonyms()

    print(f'Definition {i} : {definition}')
    print(f'Lemma Name {i} : {lemma_name}')
    print(f'Hyponyms {i} : {hyponyms}')
    print(f'Hypernyms {i} : {hypernyms}')
    print(f'Part Meronyms {i} : {part_meronyms}')
    print("**********************************************")
    i += 1

