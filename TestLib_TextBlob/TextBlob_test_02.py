from textblob import TextBlob
from textblob.classifiers import NaiveBayesClassifier

train = [
     ("N'aime pas les autres", "neg"),
     ("Faire le mal", "neg"),
     ("Le monde est mauvaise", "neg"),
     ("J'aime pas les gens", "neg"),

     ("Le monde est magnifique", "pos"),
     ("Faire le bien", "pos"),
     ("Faire l'amour", "pos"),
     ("Aimer son prochain", "pos"),
]

cl = NaiveBayesClassifier(train)
text = "Aimer faire du python"
# text = text.lower()
print(cl.classify(text))

message = TextBlob("J'aime pas le VBA")
print(message.sentiment)

'''
train = [
     ("I love life", 'pos'),
     ("This day is beautiful", 'pos'),
     ('love is good', 'pos'),
     ("All's well That ends well", 'pos'),
     ("fly in the sky is fine"),

     ("It's a fail", 'neg'),
     ("I do not like it", 'neg'),
     ("evil is wicked", 'neg'),
     ("Pirates are bad", 'neg'),
     ("stealing from someone is not good", 'neg')
]
'''