from textblob import TextBlob
from textblob.classifiers import NaiveBayesClassifier
# import nltk

train = [
     ("j'aime la vie", 'pos'),
     ("Cette journée est magnifique", 'pos'),
     ("L'amour c'est bien", 'pos'),
     ('Le bien est une finalité', 'pos'),
     ("Tout est bien qui fini bien", 'pos'),

     ("les moutons mange de l'herbe", "neu"),
     ("Ce message est neutre", 'neu'),
     ("L'eau mouille, le feu brule", 'neu'),

     ("C'est un echec", 'neg'),
     ("Je n'aime pas ça", 'neg'),
     ("Le mal est méchant", 'neg'),
     ("Les pirates sont mauvais", 'neg'),
     ("voler c'est pas bien", 'neg')
]

cl = NaiveBayesClassifier(train)
text_2 = "les moutons ne mange pas de viande"
text = "La vie est belle !!!"
# text = text.lower()

print(cl.classify(text))


message = TextBlob("Les pirates sont mauvais")
print(message.sentiment)


