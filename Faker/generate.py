from faker import Faker
from snorkel.labeling import labeling_function
from textblob import TextBlob
from textblob_fr import PatternTagger, PatternAnalyzer
import random
import csv


ABSTAIN = -1
NOT_SPAM = 0
SPAM = 1

DATA = []

f = Faker()

List_Films = ["Batman", "Star Wars", "Matrix"]
List_comment = ["Ce film est géniale",
                "Tout simplement mauvais",
                "J\'ai adoré ce film",
                "Dans l\'ensemble j\'ai passé un bon moment",
                "Le meilleur film que j\'ai vu jusqu\'a maintenant",
                "Trop null, je me suis ennuyé tout du long !",
                "Moyen, ce n\'est pas le meilleur que j\'ai vu",
                "Un films mythique !!! ",
                "j\'ai adoré. Trop drôle comme film !",
                "Je n\'aime pas du tout c\'est trop chiant"]



@labeling_function()
def lf_textblob_polarity(Comment):
    if TextBlob(Comment, pos_tagger=PatternTagger(), analyzer=PatternAnalyzer()).sentiment[0] > 0.3:
        return SPAM
    elif TextBlob(Comment, pos_tagger=PatternTagger(), analyzer=PatternAnalyzer()).sentiment[0] < 0.3 and TextBlob(Comment, pos_tagger=PatternTagger(), analyzer=PatternAnalyzer()).sentiment[0] > -0.3:
        return NOT_SPAM
    else:
        return ABSTAIN


for _ in range(500):
    Name = f.name()
    Date = f.date()
    Films = random.choice(List_Films)
    Comment = random.choice(List_comment)
    blob = TextBlob(Comment, pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
    Label = lf_textblob_polarity(Comment)

    DATA.append([Name, Date, Comment, Label, Films])


with open('generate_data.csv', 'w') as f:
    data = csv.writer(f, delimiter=',')
    data.writerow(['author', 'date', 'text', 'label', 'video'])
    for ele in DATA:
        data.writerow(ele)

