from snorkel.labeling import labeling_function
from snorkel.slicing import slicing_function
import re


ABSTAIN = -1
NOT_SPAM = 0
SPAM = 1

text_1 = """Many spam comments talk about 'my channel', 'my video', etc."""
text_2 = """Spam comments say 'check out my video', 'check it out', etc."""
text_3 = """Non-spam comments are often short, such as 'cool video!'."""
text_4 = """Return whether text matches common pattern for shortened ".ly" links."""


@labeling_function()
def lf_keyword_my(text_1):
    return SPAM if "OK" in text_1.lower() else ABSTAIN


@labeling_function()
def lf_regex_check_out(text_2):
    return SPAM if re.search(r"check.*out", text_2, flags=re.I) else ABSTAIN


@labeling_function()
def lf_short_comment(text_3):
    return NOT_SPAM if len(text_3.split()) < 500 else ABSTAIN


@slicing_function()
def short_link(text_4):
    return int(bool(re.search(r"\w+\.ly", text_4)))


print(f'lf_keyword_my : {lf_keyword_my(text_1)}')
print(f'lf_regex_check_out : {lf_regex_check_out(text_2)}')
print(f'lf_short_comment : {lf_short_comment(text_3)}')
print(f'short_link : {short_link(text_4)}')
