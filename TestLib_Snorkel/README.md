# Test librairie Snorkel

## Data
>generate_data.csv  
* Base de données de commentaire de films  
* Base de données fictives et généré avec la librairie Faker  

Description  

|   Variables  |                       Descriptions                                |
|:------------ |:----------------------------------------------------------------- |
| author       | Noms et prénoms de l'auteur du commentaire                        |
| date         | Date à laquel l'auteur à posté le commentaire                     |
| text         | Commentaire du film                                               |
| label        | Score de sentiment (polarité du commentaire)                      |
|              | 1 si le commentaire est positif soit compris entre [0,3 ; 1]      | 
|              | 0 si le commentaire est neutre soit compris entre [-0,3 ; 0.3]    |
|              | -1 si le commentaire est négatif soit compris entre [-1 ; -0.3]   |
| video        | Titre du films                                                    |  



>text_classification.csv  
* Source :  

Description  

|   Variables  |                       Descriptions                                |
|:------------ |:----------------------------------------------------------------- |
| articles     | Thèmes des articles (Baseball, Tennis, Judo ...)                  |
| links        | Liens des articles wikipedia                                      |
| content      | Contenus du commentaire                                           |
| class        | Catégories des articles (sport, person, food...)                  |  



>sentences.csv  
* Commentaire du manuel de Snorkel  

Description  

|   Variables  |                       Descriptions                                |
|:------------ |:----------------------------------------------------------------- |
| sentences    | Commentaire...                                                    |  



>unlabelled-dataset.csv  
* Source : https://www.section.io/engineering-education/snorkel-python-for-labeling-datasets-programmatically/  
* Author : Charles Kariuki  

Description  

|   Variables  |                       Descriptions                                |
|:------------ |:----------------------------------------------------------------- |
| sentences    | Commentaire...                                                    |  


