import pandas as pd
from sklearn.model_selection import train_test_split
from snorkel.preprocess import preprocessor
from snorkel.labeling import labeling_function
from snorkel.labeling import PandasLFApplier
from snorkel.labeling import LFAnalysis
from textblob import TextBlob
from textblob_fr import PatternTagger, PatternAnalyzer

df = pd.read_csv("data/generate_data.csv")
df_train, df_test = train_test_split(df, train_size=0.5)

ABSTAIN = -1
HAM = 0
SPAM = 1

@preprocessor(memoize=True)
def textblob_sentiment(x):
    scores = TextBlob(x.text, pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
    x.polarity = scores.sentiment[0]
    x.subjectivity = scores.sentiment[1]
    return x

@labeling_function(pre=[textblob_sentiment])
def textblob_polarity(x):
    return HAM if x.polarity > 0.3 else ABSTAIN

@labeling_function(pre=[textblob_sentiment])
def textblob_subjectivity(x):
    return HAM if x.subjectivity >= 0.4 else ABSTAIN

lfs = [textblob_polarity, textblob_subjectivity]

applier = PandasLFApplier(lfs)
L_train = applier.apply(df_train)
analyse = LFAnalysis(L_train, lfs).lf_summary()
print(analyse)



