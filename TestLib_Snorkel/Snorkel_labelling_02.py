# Source : https://www.section.io/engineering-education/snorkel-python-for-labeling-datasets-programmatically/
# Author : Charles Kariuki

import pandas as pd
import glob
import os
import functools
import re

from sklearn.model_selection import train_test_split
from snorkel.labeling import labeling_function
from snorkel.labeling import PandasLFApplier
from snorkel.labeling.model import LabelModel

files_joined_page = os.path.join("data/unlabelled-dataset.csv")
list_files_page = glob.glob(files_joined_page)

df = pd.concat(map(functools.partial(pd.read_csv, sep=';'), list_files_page))
'''print(df)'''

df_train, df_test = train_test_split(df, train_size=0.5)
print(df_train.shape)

QUESTION = 1
ABSTAIN = -1

@labeling_function()
def lf_keyword_lookup(x):
  keywords = ["why", "what", "when", "who", "where", "how"]
  return QUESTION if any(word in x.sentences.lower() for word in keywords) else ABSTAIN

@labeling_function()
def lf_regex_contains_what(x):
  return QUESTION if re.search(r"what.*?", x.sentences, flags=re.I) else ABSTAIN

@labeling_function()
def lf_regex_contains_question_mark(x):
  return QUESTION if re.search(r".*?", x.sentences, flags=re.I) else ABSTAIN


lfs = [lf_keyword_lookup, lf_regex_contains_what, lf_regex_contains_question_mark]

applier = PandasLFApplier(lfs=lfs)
L_train = applier.apply(df=df_train)

print(L_train)

label_model = LabelModel(cardinality=2, verbose=True)
label_model.fit(L_train=L_train, n_epochs=500, log_freq=100, seed=123)

'''
Le LabelModel utilise la méthode fit() pour ajuster le modèle dans le L_train. 
L_train contient les fonctions d'étiquetage et l'ensemble de données d'apprentissage.
Au cours de cette phase, le modèle acquiert des connaissances grâce à la formation (phase d'entrainemet). 
Il utilise éventuellement les connaissances acquises pour faire des prédictions.

Nous utilisons également les paramètres suivants.
n_epochs=500 - Le nombre d'itérations que le modèle passe à travers le L_train.
cardinality=2 - Cela montre les sorties d'étiquettes possibles. Dans notre cas, nous avons 1 et -1.
verbose=True - Cela nous permet d'utiliser des expressions régulières lors de la recherche de ?.
log_freq=100 - Il vérifie la fréquence à laquelle des phrases spécifiques sont distribuées dans l'ensemble de données.
seed=123 - Nombres aléatoires que notre modèle utilisera lors de la formation du modèle.
Après 500 époques, nous aurions entraîné avec succès notre modèle.
'''

df_train['Labels'] = label_model.predict(L=L_train, tie_break_policy="abstain")
df_train.to_csv("result.csv")


'''
100%|██████████| 44/44 [00:00<00:00, 10691.08it/s]
INFO:root:Computing O...
INFO:root:Estimating \mu...
  0%|          | 0/500 [00:00<?, ?epoch/s]INFO:root:[0 epochs]: TRAIN:[loss=0.964]
  9%|▊         | 43/500 [00:00<00:04, 99.89epoch/s]INFO:root:[100 epochs]: TRAIN:[loss=0.001]
 29%|██▉       | 147/500 [00:00<00:01, 334.38epoch/s]INFO:root:[200 epochs]: TRAIN:[loss=0.000]
 50%|████▉     | 249/500 [00:00<00:00, 514.10epoch/s]INFO:root:[300 epochs]: TRAIN:[loss=0.000]
 71%|███████   | 355/500 [00:00<00:00, 662.21epoch/s]INFO:root:[400 epochs]: TRAIN:[loss=0.000]
100%|██████████| 500/500 [00:01<00:00, 495.11epoch/s]
INFO:root:Finished Training
'''


