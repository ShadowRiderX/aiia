import random

import nltk
from nltk.corpus import wordnet as wn

from snorkel.augmentation import transformation_function

nltk.download("wordnet", quiet=True)

word ='book'
def get_synonyms(word):
    lemmas = set().union(*[s.lemmas() for s in wn.synsets(word)])
    return list(set(l.name().lower().replace("_", " ") for l in lemmas) - {word})
print(f'get_synonyms : {get_synonyms(word)}')


text ='book'
@transformation_function()
def tf_replace_word_with_synonym(text):
    words = text.lower().split()
    idx = random.choice(range(len(words)))
    synonyms = get_synonyms(words[idx])
    if len(synonyms) > 0:
        text = " ".join(words[:idx] + [synonyms[0]] + words[idx + 1 :])
        return text
print(f'tf_replace_word_with_synonym : {tf_replace_word_with_synonym(text)}')
