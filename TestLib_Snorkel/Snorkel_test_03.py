from snorkel.labeling import labeling_function
from textblob import TextBlob
from textblob_fr import PatternTagger, PatternAnalyzer

ABSTAIN = -1
NOT_SPAM = 0
SPAM = 1

text = """
    J'aime l'enfer car il fait chaud ! ho ho ho !
    """

blob = TextBlob(text, pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())

print(blob.sentiment)
print(blob.sentiment[0])
print(blob.sentiment[1])

print(blob.polarity)
print(blob.subjectivity)

@labeling_function()
def lf_textblob_polarity(text):
    return SPAM if TextBlob(text,
                            pos_tagger=PatternTagger(),
                            analyzer=PatternAnalyzer()).sentiment[0] > 0.3 else ABSTAIN

print(f'lf_textblob_polarity : {lf_textblob_polarity(text)}')


