import pandas as pd

from sklearn.model_selection import train_test_split
from snorkel.labeling import labeling_function
from snorkel.labeling import PandasLFApplier
from snorkel.labeling import LFAnalysis
from snorkel.analysis import get_label_buckets

df = pd.read_csv("data/generate_data.csv")
df_train, df_test = train_test_split(df, train_size=0.5)

Y_test = df_test.label.values

ABSTAIN = -1
HAM = 0
SPAM = 1

A = df_train[["author", "text", "video"]].sample(20, random_state=2)
print(A)

@labeling_function()
def good(x):
    keywords = ["géniale", "adoré", "j'aime", "j'adore"]
    return SPAM if any(word in str(x.text).lower() for word in keywords) else ABSTAIN

@labeling_function()
def bad(x):
    keywords = ["mauvais", "null", "j'aime pas", "ennuyé"]
    return ABSTAIN if any(word in str(x.text).lower() for word in keywords) else SPAM

lfs = [good, bad]

applier = PandasLFApplier(lfs=lfs)
L_train = applier.apply(df=df_train)
print(L_train)


coverage_good, coverage_bad = (L_train != ABSTAIN).mean(axis=0)
print(f"coverage_good coverage: {coverage_good * 100:.1f}%")
print(f"coverage_bad coverage: {coverage_bad * 100:.1f}%")


lf_stat = LFAnalysis(L=L_train, lfs=lfs).lf_summary()
print(lf_stat)

tab_1 = df_train.iloc[L_train[:, 1] == SPAM].sample(10, random_state=1)
print(tab_1)

buckets = get_label_buckets(L_train[:, 0], L_train[:, 1])
tab_2 = df_train.iloc[buckets[(ABSTAIN, SPAM)]].sample(10, random_state=1)
print(tab_2)



