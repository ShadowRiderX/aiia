from snorkel.labeling import labeling_function
from textblob import TextBlob

ABSTAIN = -1
NOT_SPAM = 0
SPAM = 1

text = """
    We use a third-party sentiment classification model, TextBlob.
    We combine this with the heuristic that non-spam comments are often positive.
    """

@labeling_function()
def lf_textblob_polarity(text):
    return NOT_SPAM if TextBlob(text).sentiment.polarity > 0.3 else ABSTAIN

print(f'lf_textblob_polarity : {lf_textblob_polarity(text)}')

