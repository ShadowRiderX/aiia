import re
import json



custom_stopwords = set()
with open('dictionnaires/stopwords_FR.csv', 'r') as file:
    for line in file:
        custom_stopwords.add(line.strip().lower())


REMPLACE_SANS_ESPACE = re.compile("[;:!?,\"()]")
REMPLACE_AVEC_ESPACE = re.compile("[.']")


def preprocess(prompt):
    prompt = prompt.lower()
    prompt = REMPLACE_SANS_ESPACE.sub("", prompt)
    prompt = REMPLACE_AVEC_ESPACE.sub(" ", prompt)
    prompt = ' '.join([word for word in prompt.split() if word not in custom_stopwords])
    return prompt


with open("dictionnaires/mots_offensants.json", "r") as json_file:
    mots_offensants_data = json.load(json_file)


mots_offensants_1 = {}
mots_offensants_2 = {}
caracteristiques_retenues = set()


for categorie, mots in mots_offensants_data.items():
    if categorie != "caracteristique" and categorie != "ethnique" and isinstance(mots, list):
        for mot in mots:
            mots_offensants_1[mot.lower()] = categorie


for categorie, mots in mots_offensants_data.items():
    if categorie != "caracteristique" and isinstance(mots, list):
        for mot in mots:
            mots_offensants_2[mot.lower()] = categorie
    else:
        for mot in mots:
            caracteristiques_retenues.add(mot.lower())


def score_impolitesse(prompt):
    score_impolitesse = 0
    for i, token in enumerate(prompt.split()):
        if token.lower() in mots_offensants_1:
            categorie = mots_offensants_1[token.lower()]
            if categorie:
                score_impolitesse += -10
                print(categorie)
                print(token)
        if token.lower() in caracteristiques_retenues:
            if i > 0 and prompt.split()[i - 1].lower() in mots_offensants_2:
                score_impolitesse += -10
                print(token)
            elif i < len(prompt.split()) - 1 and prompt.split()[i + 1].lower() in mots_offensants_2:
                score_impolitesse += -10
                print(token)
    score_impolitesse = min(max(score_impolitesse, -100), 100)
    return score_impolitesse


with open("dictionnaires/word_univers.json", "r") as json_file:
    word_univers = json.load(json_file)


def score_coherence(contexte, prompt):
    contexte = contexte.lower()
    prompt = prompt.lower()
    mots_contexte = re.findall(r'\w+', contexte)
    mots_utilises = []
    for mot in word_univers:
        mot = mot.lower()
        if mot in prompt:
            mots_utilises.append(mot)

    for mots_associes in word_univers.values():
        for mot_associe in mots_associes:
            mot_associe = mot_associe.lower()
            if mot_associe in prompt:
                mots_utilises.append(mot_associe)

    for mot, mots_associes in word_univers.items():
        mot = mot.lower()
        if mot in mots_contexte or any(mot_associe in mots_contexte for mot_associe in mots_associes):
            mots_a_comparer = [mot] + mots_associes
            if any(mot_a_comparer in prompt for mot_a_comparer in mots_a_comparer):
                score_retenu = len(mots_utilises)
                indicateur = f"nombre de mots {score_retenu} : {mots_utilises}"
                return "oui", indicateur, score_retenu

    return "non", "", 0





contexte = "Aujourd'hui nous allons voir en détails les techniques de piano !"


while True:
    print("**************** INPUT ****************")
    print(contexte)
    prompt = input("Saisissez une phrase (ou tapez 'exit' pour quitter) : ")
    print("***************************************")

    if prompt.lower() == 'exit':
        break

    valide, indicateur, score_retenu = score_coherence(contexte, prompt)
    prompt = preprocess(prompt)
    score = score_impolitesse(prompt)

    if score < 0:
        commentaire = "Votre message contient des propos impolis ou offensants."
    elif score == 0:
        commentaire = "Votre message est neutre et respecte les règles de modération."
    else:
        commentaire = "Votre message est poli et respectueux."

    print("*************** ANALYSE ***************")
    print(f"Valide : {valide}")
    print(f"Score de cohérence : {score_retenu}")
    if valide == "oui":
        print(indicateur)
    print(f"Commentaire : {commentaire}")
    print(f"Score d'impolitesse : {score}")
    print("***************************************")
    print("\n\n")



