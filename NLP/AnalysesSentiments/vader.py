from flask import Flask, render_template
import nltk
from vaderSentiment_fr.vaderSentiment import SentimentIntensityAnalyzer

app = Flask(__name__)

analyzer = SentimentIntensityAnalyzer()

def load_text_from_file(filename):
    with open(filename, 'r', encoding='utf-8') as file:
        text = file.read()
    return text

def analyze_text(text):
    sentences = nltk.sent_tokenize(text)
    analysis = []

    types_summary = {'Interrogative': 0, 'Exclamative': 0, 'Neutre': 0}
    polarity_summary = {'Positive': 0, 'Negative': 0, 'Neutre': 0}
    intensity_summary = {'Fort': 0, 'Faible': 0}

    for i, sentence in enumerate(sentences):
        if sentence.endswith('?'):
            sentence_type = 'Interrogative'
        elif sentence.endswith('!'):
            sentence_type = 'Exclamative'
        else:
            sentence_type = 'Neutre'

        scores = analyzer.polarity_scores(sentence)
        positive_score = scores['pos']
        negative_score = scores['neg']
        neutral_score = scores['neu']
        intensity = scores['compound']

        types_summary[sentence_type] += 1

        if positive_score:
            polarity_text = f'Positive ({positive_score:.3f})'
        elif negative_score:
            polarity_text = f'Negative (-{negative_score:.3f})'
        else:
            polarity_text = f'Neutre ({neutral_score-1:.3f})'

        if intensity > 0.5:
            intensity_text = 'Fort'
        else:
            intensity_text = 'Faible'

        analysis.append((i + 1, sentence_type, polarity_text, intensity_text + f' ({intensity:.4f})'))

        if positive_score:
            polarity_summary['Positive'] += 1
        elif negative_score:
            polarity_summary['Negative'] += 1
        else:
            polarity_summary['Neutre'] += 1

        if intensity > 0.4:
            intensity_summary['Fort'] += 1
        else:
            intensity_summary['Faible'] += 1

    return analysis, types_summary, polarity_summary, intensity_summary

@app.route('/')
def index():
    filename = 'votre_fichier.txt'
    text = load_text_from_file(filename)
    analysis, types_summary, polarity_summary, intensity_summary = analyze_text(text)
    return render_template('index_VADER.html', analysis=analysis, types_summary=types_summary,
                           polarity_summary=polarity_summary, intensity_summary=intensity_summary)

if __name__ == '__main__':
    app.run(debug=True)
