from flask import Flask, render_template
import nltk
from vaderSentiment_fr.vaderSentiment import SentimentIntensityAnalyzer
from textblob import Blobber
from textblob_fr import PatternTagger, PatternAnalyzer

app = Flask(__name__)

analyzer = SentimentIntensityAnalyzer()
tb = Blobber(pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())


def load_text_from_file(filename):
    with open(filename, 'r', encoding='utf-8') as file:
        text = file.read()
    return text


def analyze_text(text):
    sentences = nltk.sent_tokenize(text)
    analysis = []

    types_summary = {'Interrogative': 0, 'Exclamative': 0, 'Neutre': 0}
    polarity_summary_textblob = {'Positive': 0, 'Negative': 0, 'Neutre': 0}
    polarity_summary_vader = {'Positive': 0, 'Negative': 0, 'Neutre': 0}
    intensity_summary = {'Forte': 0, 'Faible': 0}
    subjectivity_summary = {'Objective': 0, 'Subjective': 0}

    for i, sentence in enumerate(sentences):
        if sentence.endswith('?'):
            sentence_type = 'Interrogative'
        elif sentence.endswith('!'):
            sentence_type = 'Exclamative'
        else:
            sentence_type = 'Neutre'

        blob = tb(sentence)
        polarity = blob.sentiment[0]
        subjectivity = blob.sentiment[1]

        types_summary[sentence_type] += 1

        if polarity > 0.05:
            polarity_text_tb = 'Positive'
        elif polarity <= -0.05:
            polarity_text_tb = 'Negative'
        else:
            polarity_text_tb = 'Neutre'

        if subjectivity > 0.5:
            subjectivity_text = 'Subjective'
        else:
            subjectivity_text = 'Objective'

        polarity_summary_textblob[polarity_text_tb] += 1
        subjectivity_summary[subjectivity_text] += 1

        scores = analyzer.polarity_scores(sentence)
        positive_score = scores['pos']
        negative_score = scores['neg']
        neutral_score = scores['neu']
        intensity = scores['compound']

        if positive_score:
            polarity_text_vader = f'Positive ({positive_score:.4f})'
            polarity_summary_vader['Positive'] += 1
        elif negative_score:
            polarity_text_vader = f'Negative (-{negative_score:.4f})'
            polarity_summary_vader['Negative'] += 1
        else:
            polarity_text_vader = f'Neutre ({neutral_score - 1:.4f})'
            polarity_summary_vader['Neutre'] += 1

        if intensity > 0.4:
            intensity_text = 'Forte'
        else:
            intensity_text = 'Faible'

        intensity_summary[intensity_text] += 1

        analysis.append((i + 1, sentence_type, f'{polarity_text_tb} ({polarity:.4f})', polarity_text_vader,
                         intensity_text + f' ({intensity:.4f})', subjectivity_text + f' ({subjectivity:.4f})'))

    return analysis, types_summary, polarity_summary_textblob, polarity_summary_vader, intensity_summary, subjectivity_summary


@app.route('/')
def index():
    filename = 'votre_fichier.txt'
    text = load_text_from_file(filename)
    analysis, types_summary, polarity_summary_textblob, polarity_summary_vader, intensity_summary, subjectivity_summary = analyze_text(text)
    return render_template('index.html', analysis=analysis, types_summary=types_summary,
                           polarity_summary_textblob=polarity_summary_textblob,
                           polarity_summary_vader=polarity_summary_vader,
                           intensity_summary=intensity_summary, subjectivity_summary=subjectivity_summary)


if __name__ == '__main__':
    app.run(debug=True)
