from flask import Flask, render_template
import nltk
from textblob import Blobber
from textblob_fr import PatternTagger, PatternAnalyzer
tb = Blobber(pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())

app = Flask(__name__)

def load_text_from_file(filename):
    with open(filename, 'r', encoding='utf-8') as file:
        text = file.read()
    return text

def analyze_text(text):
    sentences = nltk.sent_tokenize(text)
    analysis = []

    types_summary = {'Interrogative': 0, 'Exclamative': 0, 'Neutre': 0}
    polarity_summary = {'Positive': 0, 'Negative': 0, 'Neutre': 0}
    subjectivity_summary = {'Objective': 0, 'Subjective': 0}

    for i, sentence in enumerate(sentences):
        if sentence.endswith('?'):
            sentence_type = 'Interrogative'
        elif sentence.endswith('!'):
            sentence_type = 'Exclamative'
        else:
            sentence_type = 'Neutre'

        blob = tb(sentence)
        polarity = blob.sentiment[0]
        subjectivity = blob.sentiment[1]

        types_summary[sentence_type] += 1

        if polarity > 0.05:
            polarity_text = 'Positive'
        elif polarity <= -0.05:
            polarity_text = 'Negative'
        else:
            polarity_text = 'Neutre'

        if subjectivity > 0.5:
            subjectivity_text = 'Subjectif'
        else:
            subjectivity_text = 'Objectif'

        analysis.append((i + 1, sentence_type, polarity_text + f' ({polarity:.4f})', subjectivity_text + f' ({subjectivity:.4f})'))

        if polarity > 0:
            polarity_summary['Positive'] += 1
        elif polarity < 0:
            polarity_summary['Negative'] += 1
        else:
            polarity_summary['Neutre'] += 1

        if subjectivity > 0.5:
            subjectivity_summary['Subjective'] += 1
        else:
            subjectivity_summary['Objective'] += 1

    return analysis, types_summary, polarity_summary, subjectivity_summary

@app.route('/')
def index():
    filename = 'votre_fichier.txt'
    text = load_text_from_file(filename)
    analysis, types_summary, polarity_summary, subjectivity_summary = analyze_text(text)
    return render_template('index_TEXTBLOB.html', analysis=analysis, types_summary=types_summary,
                                  polarity_summary=polarity_summary, subjectivity_summary=subjectivity_summary)

if __name__ == '__main__':
    app.run(debug=True)
